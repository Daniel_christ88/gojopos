<?php

use App\Http\Controllers\PurchaseOrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\IngredientController;
use App\Http\Controllers\InOutStockController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\SellingTagController;
use App\Http\Controllers\StockLogController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\WarehouseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::middleware('auth:api')->group(function() {
  Route::group(['prefix' => 'user'], function () {
    Route::get('current', [AuthController::class, 'getCurrentUser']);
  });

  Route::group(['prefix' => 'brand'], function () {
    Route::get('/', [BrandController::class, 'index']);
    Route::get('{brand}/show', [BrandController::class, 'show']);
    Route::post('/', [BrandController::class, 'store']);
    Route::put('{brand}', [BrandController::class, 'update']);
    Route::delete('{brand}', [BrandController::class, 'destroy']);
  });

  Route::prefix('ingredient')->group(function () {
    Route::get('/', [IngredientController::class, 'index']);
    Route::get('/most-bought', [IngredientController::class, 'getMostBoughtIngredient']);
    Route::get('/total-bought', [IngredientController::class, 'getTotalBought']);
    Route::get('{ingredient}/show', [IngredientController::class, 'show']);
    Route::post('/', [IngredientController::class, 'store']);
    Route::put('{ingredient}', [IngredientController::class, 'update']);
    Route::delete('{ingredient}', [IngredientController::class, 'destroy']);
  });

  Route::prefix('warehouse')->group(function () {
    Route::get('/', [WarehouseController::class, 'index']);
    Route::get('{warehouse}/show', [WarehouseController::class, 'show']);
    Route::post('/', [WarehouseController::class, 'store']);
    Route::put('{warehouse}', [WarehouseController::class, 'update']);
    Route::delete('{warehouse}', [WarehouseController::class, 'destroy']);
  });

  Route::prefix('selling-tag')->group(function () {
    Route::get('/', [SellingTagController::class, 'index']);
    Route::get('{sellingTag}/show', [SellingTagController::class, 'show']);
    Route::post('/', [SellingTagController::class, 'store']);
    Route::put('{sellingTag}', [SellingTagController::class, 'update']);
    Route::delete('{sellingTag}', [SellingTagController::class, 'destroy']);
  });

  Route::prefix('supplier')->group(function () {
    Route::get('/', [SupplierController::class, 'index']);
    Route::get('{supplier}/show', [SupplierController::class, 'show']);
    Route::post('/', [SupplierController::class, 'store']);
    Route::put('{supplier}', [SupplierController::class, 'update']);
    Route::delete('{supplier}', [SupplierController::class, 'destroy']);
  });

  Route::prefix('product')->group(function () {
    Route::get('/', [ProductController::class, 'index']);
    Route::get('/most-sold', [ProductController::class, 'getMostSold']);
    Route::get('/total-sold', [ProductController::class, 'getTotalSold']);
    Route::get('{product}/show', [ProductController::class, 'show']);
    Route::get('get-product-number', [ProductController::class, 'getProductNumber']);
    Route::post('/', [ProductController::class, 'store']);
    Route::post('/upload-image', [ProductController::class, 'uploadImage']);
    Route::put('{product}', [ProductController::class, 'update']);
    Route::delete('{product}', [ProductController::class, 'destroy']);
  });

  Route::prefix('purchase-order') -> group( function () {
      Route::get('/', [PurchaseOrderController::class, 'index']);
      Route::get('/summary', [PurchaseOrderController::class, 'getSummary']);
      Route::get('{purchaseOrder}/show', [PurchaseOrderController::class, 'show']);
      Route::get('{purchaseOrder}/show/ingredients', [PurchaseOrderController::class, 'showIngredients']);
      Route::get('get-purchase-order-number', [PurchaseOrderController::class, 'getPurchaseOrderNumber']);
      Route::post('/', [PurchaseOrderController::class, 'store']);
      Route::put('{purchaseOrder}/change-status', [PurchaseOrderController::class, 'changeStatus']);
      Route::put('{purchaseOrder}', [PurchaseOrderController::class, 'update']);
  });

  Route::prefix('promotion')->group(function () {
    Route::get('/', [PromotionController::class, 'index']);
    Route::get('{promotion}/show', [PromotionController::class, 'show']);
    Route::post('/', [PromotionController::class, 'store']);
    Route::put('{promotion}', [PromotionController::class, 'update']);
    Route::delete('{promotion}', [PromotionController::class, 'destroy']);
  });

  Route::prefix('transaction')->group(function () {
    Route::get('/', [TransactionController::class, 'index']);
    Route::get('/summary', [TransactionController::class, 'getSummary']);
    Route::get('{transaction}/show', [TransactionController::class, 'show']);
    Route::get('{transactionNumber}/show-by-number', [TransactionController::class, 'showByNumber']);
    Route::get('get-transaction-number', [TransactionController::class, 'getTransactionNumber']);
    Route::post('check-stock-product', [TransactionController::class, 'checkStockProduct']);
    Route::post('approve-minus-stock', [TransactionController::class, 'approveMinusStock']);
    Route::post('/', [TransactionController::class, 'store']);
    Route::put('{transaction}', [TransactionController::class, 'update']);
    Route::put('{transaction}/confirm-payment', [TransactionController::class, 'confirmPayment']);
    Route::put('{transaction}/select-order', [TransactionController::class, 'selectOrder']);
    Route::delete('{transaction}', [TransactionController::class, 'destroy']);
  });

  Route::prefix('customer')->group(function () {
    Route::get('/', [CustomerController::class, 'index']);
    Route::get('{customer}/show', [CustomerController::class, 'show']);
    Route::post('/', [CustomerController::class, 'store']);
    Route::put('{customer}', [CustomerController::class, 'update']);
    Route::delete('{customer}', [CustomerController::class, 'destroy']);
  });

  Route::prefix('company')->group(function () {
    Route::get('/', [CompanyController::class, 'index']);
    Route::get('{company}/show', [CompanyController::class, 'show']);
    Route::post('/', [CompanyController::class, 'store']);
    Route::put('{company}', [CompanyController::class, 'update']);
    Route::delete('{company}', [CompanyController::class, 'destroy']);
  });

  Route::prefix('in-out-stock')->group(function () {
    Route::get('/', [InOutStockController::class, 'index']);
    Route::get('{inOutStock}/show', [InOutStockController::class, 'show']);
    Route::get('get-in-out-stock-number', [InOutStockController::class, 'getInOutStockNumber']);
    Route::post('/', [InOutStockController::class, 'store']);
    Route::put('{inOutStock}', [InOutStockController::class, 'update']);
    Route::delete('{inOutStock}', [InOutStockController::class, 'destroy']);
  });

  Route::prefix('stock-log')->group(function () {
    Route::get('/', [StockLogController::class, 'index']);
  });
});
