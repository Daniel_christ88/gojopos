<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IngredientPurchaseOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['ingredient']['id'],
            'name' => $this['ingredient']['name'],
            'stock' => $this['ingredient']['stock'],
            'quantity' => $this->quantity,
            'price' => $this->price,
        ];
    }
}
