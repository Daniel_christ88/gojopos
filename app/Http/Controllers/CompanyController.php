<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Services\CompanyService;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected $company;
    protected $companyService;

    public function __construct(Company $company, CompanyService $companyService)
    {
        $this->company = $company;
        $this->companyService = $companyService;
    }

    public function index(Request $request)
    {
        return $this->companyService->getCompanys($request);
    }

    public function store(Request $request)
    {
        return $this->companyService->createCompany($request);
    }

    public function update(Company $company, Request $request)
    {
        return $this->companyService->updateCompany($company, $request);
    }

    public function destroy(Company $company)
    {
        return $this->companyService->destroyCompany($company);
    }

    public function show(Company $company)
    {
        $company = Company::with(['company' ])
            ->find($company->id);

        return response()->json($company, 200);
    }
}
