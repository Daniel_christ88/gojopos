<?php

namespace App\Http\Controllers;

use App\Models\SellingTag;
use App\Services\SellingTagService;
use Illuminate\Http\Request;

class SellingTagController extends Controller
{
    protected $sellingTag;
    protected $sellingTagService;

    public function __construct(SellingTag $sellingTag, SellingTagService $sellingTagService)
    {
        $this->sellingTag = $sellingTag;
        $this->sellingTagService = $sellingTagService;        
    }

    public function index(Request $request)
    {
        return $this->sellingTagService->getSellingTags($request);
    }

    public function store(Request $request)
    {
        return $this->sellingTagService->createSellingTag($request);
    }

    public function update(SellingTag $sellingTag, Request $request)
    {
        return $this->sellingTagService->updateSellingTag($sellingTag, $request);
    }

    public function destroy(SellingTag $sellingTag)
    {
        return $this->sellingTagService->destroySellingTag($sellingTag);
    }

    public function show(SellingTag $sellingTag)
    {
        return response()->json($sellingTag, 200);
    }
}
