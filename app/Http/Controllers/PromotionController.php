<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use App\Services\PromotionService;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    protected $promotion;
    protected $promotionService;

    public function __construct(Promotion $promotion, PromotionService $promotionService)
    {
        $this->promotion = $promotion;
        $this->promotionService = $promotionService;
    }

    public function index(Request $request)
    {
        return $this->promotionService->getPromotions($request);
    }

    public function store(Request $request)
    {
        return $this->promotionService->createPromotion($request);
    }

    public function update(Promotion $promotion, Request $request)
    {
        return $this->promotionService->updatePromotion($promotion, $request);
    }

    public function destroy(Promotion $promotion)
    {
        return $this->promotionService->destroyPromotion($promotion);
    }

    public function show(Promotion $promotion)
    {
        return response()->json($promotion, 200);
    }
}
