<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Services\TransactionService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    protected $transaction;
    protected $transactionService;

    public function __construct(Transaction $transaction, TransactionService $transactionService)
    {
        $this->transaction = $transaction;
        $this->transactionService = $transactionService;
    }

    public function index(Request $request)
    {
        return $this->transactionService->getTransactions($request);
    }

    public function store(Request $request)
    {
        return $this->transactionService->createTransaction($request);
    }

    public function update(Transaction $transaction, Request $request)
    {
        return $this->transactionService->updateTransaction($transaction, $request);
    }

    public function destroy(Transaction $transaction)
    {
        return $this->transactionService->destroyTransaction($transaction);
    }

    public function show(Transaction $transaction)
    {
        return response()->json($transaction, 200);
    }

    public function getTransactionNumber(Request $request)
    {
        return response()->json($this->transactionService->getTransactionNumber($request), 200);
    }

    public function checkStockProduct(Request $request)
    {
        return $this->transactionService->checkStockProduct($request);
    }

    public function approveMinusStock(Request $request)
    {
        return $this->transactionService->approveMinusStock($request);
    }

    public function showByNumber(Request $request)
    {
        return $this->transactionService->getTransactionByNumber($request);
    }

    public function confirmPayment(Transaction $transaction, Request $request)
    {
        return $this->transactionService->confirmPayment($transaction, $request);
    }

    public function selectOrder(Transaction $transaction, Request $request)
    {
        return $this->transactionService->selectOrder($transaction, $request);
    }

    public function getSummary(Request $request){
        return $this->transactionService->getSummary($request);
    }
}
