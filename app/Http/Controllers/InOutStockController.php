<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InOutStock;
use App\Services\InOutStockService;

class InOutStockController extends Controller
{
    protected $inOutStock;
    protected $inOutStockService;

    public function __construct(InOutStock $inOutStock, InOutStockService $inOutStockService)
    {
        $this->inOutStock = $inOutStock;
        $this->inOutStockService = $inOutStockService;
    }

    public function index(Request $request)
    {
        return $this->inOutStockService->getInOutStocks($request);
    }

    public function store(Request $request)
    {
        return $this->inOutStockService->createInOutStock($request);
    }

    public function update(InOutStock $inOutStock, Request $request)
    {
        return $this->inOutStockService->updateInOutStock($inOutStock, $request);
    }

    public function destroy(InOutStock $inOutStock)
    {
        return $this->inOutStockService->destroyInOutStock($inOutStock);
    }

    public function show(InOutStock $inOutStock)
    {
        $inOutStock = InOutStock::with(['ingredients'])
            ->find($inOutStock->id);

        return response()->json($inOutStock, 200);
    }

    public function getInOutStockNumber(Request $request)
    {
        return response()->json($this->inOutStockService->getInOutStockNumber($request));
    }
}
