<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Services\CustomerService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $customer;
    protected $customerService;

    public function __construct(Customer $customer, CustomerService $customerService)
    {
        $this->customer = $customer;
        $this->customerService = $customerService;
    }

    public function index(Request $request)
    {
        return $this->customerService->getCustomers($request);
    }

    public function store(Request $request)
    {
        return $this->customerService->createCustomer($request);
    }

    public function update(Customer $customer, Request $request)
    {
        return $this->customerService->updateCustomer($customer, $request);
    }

    public function destroy(Customer $customer)
    {
        return $this->customerService->destroyCustomer($customer);
    }

    public function show(Customer $customer)
    {
        $customer = Customer::with(['company' ])
            ->find($customer->id);

        return response()->json($customer, 200);
    }
}
