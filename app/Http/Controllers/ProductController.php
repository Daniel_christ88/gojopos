<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;
    protected $productService;

    public function __construct(Product $product, ProductService $productService)
    {
        $this->product = $product;
        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        return $this->productService->getProducts($request);
    }

    public function store(Request $request)
    {
        return $this->productService->createProduct($request);
    }

    public function update(Product $product, Request $request)
    {
        return $this->productService->updateProduct($product, $request);
    }

    public function destroy(Product $product)
    {
        return $this->productService->destroyProduct($product);
    }

    public function show(Product $product)
    {
        return $this->productService->getProductById($product);
    }

    public function getProductNumber(Request $request)
    {
        return response()->json($this->productService->getProductNumber($request));
    }

    public function uploadImage(Request $request)
    {
        return $this->productService->uploadImage($request);
    }

    public function getMostSold(Request $request){
        return $this->productService->getMostSold($request);
    }

    public function getTotalSold(Request $request){
        return $this->productService->totalSold($request);
    }
}
