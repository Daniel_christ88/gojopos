<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Services\BrandService;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    protected $brand;
    protected $brandService;

    public function __construct(Brand $brand, BrandService $brandService)
    {
        $this->brand = $brand;
        $this->brandService = $brandService;
    }

    public function index(Request $request)
    {
        return $this->brandService->getBrands($request);
    }

    public function store(Request $request)
    {
        return $this->brandService->createBrand($request);
    }

    public function update(Brand $brand, Request $request)
    {
        return $this->brandService->updateBrand($brand, $request);
    }

    public function destroy(Brand $brand)
    {
        return $this->brandService->destroyBrand($brand);
    }

    public function show(Brand $brand)
    {
        return response()->json($brand, 200);
    }
}
