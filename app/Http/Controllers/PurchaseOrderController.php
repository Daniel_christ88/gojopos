<?php

namespace App\Http\Controllers;

use App\Models\PurchaseOrder;
use Illuminate\Http\Request;

use App\Services\PurchaseOrderService;

class PurchaseOrderController extends Controller
{
    protected $purchaseOrder;
    protected $purchaseOrderService;

    public function __construct(PurchaseOrder $purchaseOrder, PurchaseOrderService $purchaseOrderService)
    {
        $this->purchaseOrder = $purchaseOrder;
        $this->purchaseOrderService = $purchaseOrderService;
    }

    public function index(Request $request)
    {
        return $this->purchaseOrderService->getPurchaseOrders($request);
    }

    public function store(Request $request)
    {
        return $this->purchaseOrderService->createPurchaseOrder($request);
    }

    public function update(PurchaseOrder $purchaseOrder, Request $request)
    {
        return $this->purchaseOrderService->updatePurchaseOrder($purchaseOrder, $request);
    }

    public function changeStatus(PurchaseOrder $purchaseOrder, Request $request){
        return $this->purchaseOrderService->changeStatus($purchaseOrder, $request);
    }

    public function destroy(PurchaseOrder $purchaseOrder)
    {
        return $this->purchaseOrderService->destroyPurchaseOrder($purchaseOrder);
    }

    public function show(Request $request)
    {
        return $this->purchaseOrderService->getPurchaseOrderById($request->purchaseOrder);
    }

    public function showIngredients(PurchaseOrder  $purchaseOrder){
        return $this->purchaseOrderService->getPurchaseOrderIngredients($purchaseOrder);
    }

    public function getPurchaseOrderNumber(Request $request)
    {
        return response()->json($this->purchaseOrderService->getPurchaseOrderNumber($request));
    }

    public function updatePayment(PurchaseOrder $purchaseOrder, Request $request)
    {
        return $this->purchaseOrderService->updatePayment($purchaseOrder, $request);
    }

    public function getSummary(Request $request){
       return $this->purchaseOrderService->getSummary($request);
    }
}
