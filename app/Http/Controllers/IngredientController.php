<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use App\Services\IngredientService;
use Illuminate\Http\Request;
use PDO;
use phpseclib3\Crypt\RC2;

class IngredientController extends Controller
{
    protected $ingredient;
    protected $ingredientService;

    public function __construct(Ingredient $ingredient, IngredientService $ingredientService)
    {
        $this->ingredient = $ingredient;
        $this->ingredientService = $ingredientService;
    }

    public function index(Request $request)
    {
        return $this->ingredientService->getIngredients($request);
    }

    public function store(Request $request)
    {
        return $this->ingredientService->createIngredient($request);
    }

    public function update(Ingredient $ingredient, Request $request)
    {
        return $this->ingredientService->updateIngredient($ingredient, $request);
    }

    public function destroy(Ingredient $ingredient)
    {
        return $this->ingredientService->destroyIngredient($ingredient);
    }

    public function show(Ingredient $ingredient)
    {
        $ingredient = Ingredient::with(['warehouse', 'brand'])
            ->find($ingredient->id);

        return response()->json($ingredient, 200);
    }

    public function getMostBoughtIngredient(Request $request){
        return $this->ingredientService->getMostBoughtIngredient($request);
    }

    public function getTotalBought(Request $request){
        return $this->ingredientService->totalBought($request);
    }
}
