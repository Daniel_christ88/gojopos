<?php

namespace App\Http\Controllers;

use App\Models\StockLog;
use App\Services\StockLogService;
use Illuminate\Http\Request;

class StockLogController extends Controller
{
    protected $stockLog;
    protected $stockLogService;

    public function __construct(StockLog $stockLog, StockLogService $stockLogService)
    {
        $this->stockLog = $stockLog;
        $this->stockLogService = $stockLogService;
    }

    public function index(Request $request)
    {
        return $this->stockLogService->getStockLogs($request);
    }
}
