<?php

namespace App\Http\Controllers;

use App\Models\Warehouse;
use App\Services\WarehouseService;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    protected $warehouse;
    protected $warehouseService;

    public function __construct(Warehouse $warehouse, WarehouseService $warehouseService)
    {
        $this->warehouse = $warehouse;
        $this->warehouseService = $warehouseService;
    }

    public function index(Request $request)
    {
        return $this->warehouseService->getWarehouses($request);
    }

    public function store(Request $request)
    {
        return $this->warehouseService->createWarehouse($request);
    }

    public function update(Warehouse $warehouse, Request $request)
    {
        return $this->warehouseService->updateWarehouse($warehouse, $request);
    }

    public function destroy(Warehouse $warehouse)
    {
        return $this->warehouseService->destroyWarehouse($warehouse);
    }

    public function show(Warehouse $warehouse)
    {
        return response()->json($warehouse, 200);
    }
}
