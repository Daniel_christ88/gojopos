<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use App\Services\SupplierService;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    protected $supplier;
    protected $supplierService;

    public function __construct(Supplier $supplier, SupplierService $supplierService)
    {
        $this->supplier = $supplier;
        $this->supplierService = $supplierService;
    }

    public function index(Request $request)
    {
        return $this->supplierService->getSuppliers($request);
    }

    public function store(Request $request)
    {
        return $this->supplierService->createSupplier($request);
    }

    public function update(Supplier $supplier, Request $request)
    {
        return $this->supplierService->updateSupplier($supplier, $request);
    }

    public function destroy(Supplier $supplier)
    {
        return $this->supplierService->destroySupplier($supplier);
    }

    public function show(Supplier $supplier)
    {
        return response()->json($supplier, 200);
    }
}
