<?php

namespace App\Services;

use App\Models\Ingredient;
use App\Models\InOutStock;
use App\Models\IOMovement;
use App\Models\StockLog;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;

class InOutStockService
{
    public function getInOutStocks(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $inOutStocks = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $inOutStocks = InOutStock::where('company_id', $companyId)
                    ->where('number', 'LIKE', '%' . $search . '%')
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $inOutStocks = InOutStock::where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);;
            }
        } else {
            $inOutStocks = InOutStock::where('company_id', $companyId)
                ->orderBy('id', 'DESC')
                ->get();
        }

        return response()->json($inOutStocks, 200);
    }

    public function createInOutStock(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'number' => 'required|string',
            'type' => 'required|string',
            'notes' => 'sometimes|string|nullable',

            'ingredients.*.id' => 'required|integer',
            'ingredients.*.quantity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $inOutStock = DB::transaction(function () use ($request) {

            $newInOutStock = InOutStock::create([
                'company_id' => $request->company_id,
                'number' => $request->number,
                'type' => $request->type,
                'notes' => $request->notes,
            ]);

            foreach ($request->input('ingredients') as $ingredient) {
                $isIngredientFound = Ingredient::find($ingredient['id']);
                if ($isIngredientFound->exists) {
                    IOMovement::create([
                        'ingredient_id' => $ingredient['id'],
                        'in_out_stock_id' => $newInOutStock->id,
                        'quantity' => $ingredient['quantity']
                    ]);

                    // Add To Stock Log
                    $checkIngredient = Ingredient::find($ingredient['id']);
                    StockLog::create([
                        'ingredient_id' => $ingredient['id'],
                        'type' => $request->type,
                        'quantity' => $ingredient['quantity'],
                        'remaining_stock' => $request->type == 'In' ? $checkIngredient->stock + $ingredient['quantity'] : $checkIngredient->stock - $ingredient['quantity']
                    ]);

                    if ($request->type == 'In') {
                        $checkIngredient->update([
                            'stock' => $checkIngredient->stock + $ingredient['quantity']
                        ]);
                    } else {
                        $checkIngredient->update([
                            'stock' => $checkIngredient->stock - $ingredient['quantity']
                        ]);
                    }
                }
            }

            return $newInOutStock;
        });



        return response()->json($inOutStock, 200);
    }

    public function updateInOutStock(InOutStock $inOutStock, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'number' => 'required|string',
            'type' => 'required|string',
            'notes' => 'sometimes|string|nullable',

            'ingredients.*.id' => 'required|integer',
            'ingredients.*.quantity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $result = DB::transaction(function () use ($request, $inOutStock) {
            $inOutStock->update([
                'company_id' => $request->company_id,
                'number' => $request->number,
                'type' => $request->type,
                'notes' => $request->notes,
            ]);

            IOMovement::where('in_out_stock_id', '=', $inOutStock->id)->delete();
            foreach ($request->input('ingredients') as $ingredientReq) {
                $ingredient = Ingredient::find($ingredientReq['id']);
                if ($ingredient) {
                    IOMovement::updateOrCreate(
                        [
                            'ingredient_id' => $ingredient->id,
                            'in_out_stock_id' => $inOutStock->id

                        ],
                        [

                            'quantity' => $ingredientReq['quantity']
                        ]
                    );
                }
            }
            return $inOutStock;
        });

        return response()->json($result, 200);
    }

    public function destroyInOutStock(InOutStock $inOutStock)
    {
        $inOutStock->delete();

        return response()->json($inOutStock, 200);
    }

    public function getInOutStockNumber(Request $request)
    {
        $lastInOutStock = InOutStock::where('company_id', $request->company_id)->orderBy('id', 'DESC')->first();

        if (!is_null($lastInOutStock)) {
            $number = substr($lastInOutStock->number, -6);
            $addNumber = (int) $number + 1;
            $inOutStockNumber = 'IO' . date('dmY') . sprintf("%06d", $addNumber);
        } else {
            $inOutStockNumber = 'IO' . date('dmY') . '000001';
        }

        return $inOutStockNumber;
    }
}
