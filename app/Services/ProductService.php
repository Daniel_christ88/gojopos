<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Validator;

class ProductService
{
    public function getProducts(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $products = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $products = Product::with(['sellingTagProducts.sellingTag'])
                    ->where('name', 'LIKE', '%' . $search . '%')
                    ->where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $products = Product::with(['sellingTagProducts.sellingTag'])
                    ->where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $products = Product::where('company_id', $companyId)->orderBy('name', 'ASC')->get();
        }

        return response()->json($products, 200);
    }

    public function createProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'number' => 'required|string',
            'name' => 'required|string',
            'sell_price' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $product = Product::create([
            'company_id' => $request->company_id,
            'number' => $request->number,
            'name' => $request->name,
            'description' => $request->description,
            'sell_price' => $request->sell_price
        ]);

        // Create Product Selling Tag
        foreach ($request->selling_tags as $requestSellingTag) {
            $product->sellingTagProducts()->create([
                'product_id' => $product->id,
                'selling_tag_id' => $requestSellingTag['id']
            ]);
        }

        // Create Product Ingredient
        foreach ($request->ingredients as $requestIngredient) {
            $product->productIngredients()->create([
                'product_id' => $product->id,
                'ingredient_id' => $requestIngredient['id'],
                'quantity' => $requestIngredient['quantity']
            ]);
        }

        return response()->json($product, 200);
    }

    public function uploadImage(Request $request)
    {
        $product = Product::find($request->id);
        $image = $request->image;

        if ($request->hasFile('image')) {
            if($product->image != null && \Storage::disk('public')->exists($product->image)){
                \Storage::disk('public')->delete($product->image);
            }
            $name = '/products/' . $request->id . '/' . uniqid() . '.' . $image->extension();
            $image->storePubliclyAs('public', $name);
            $product->update([
                'image' => $name
            ]);
        }

        return response()->json($product, 200);
    }

    public function updateProduct(Product $product, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'company_id' => 'required|integer',
            'number' => 'required|string',
            'name' => 'required|string',
            'sell_price' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'sell_price' => $request->sell_price
        ]);

        // Update Product Selling Tag
        foreach ($request->selling_tags as $requestSellingTag) {
            $checkSellingTagProduct = $product->sellingTagProducts()->where('selling_tag_id', $requestSellingTag['id'])->first();
            if (is_null($checkSellingTagProduct)) {
                $product->sellingTagProducts()->create([
                    'product_id' => $product->id,
                    'selling_tag_id' => $requestSellingTag['id']
                ]);
            }
        }

        $sellingTagProducts = $product->sellingTagProducts()->get();

        if (count($sellingTagProducts) > count($request->selling_tags)) {
            foreach ($sellingTagProducts as $sellingTagProduct) {
                $check = false;
                foreach ($request->selling_tags as $requestSellingTag) {
                    if ($requestSellingTag['id'] == $sellingTagProduct['id']) {
                        $check = true;
                        break;
                    }
                }
                if (!$check) {
                    $sellingTagProduct->delete();
                    $countSellingTagProducts = $product->sellingTagProducts()->count();
                    if ($countSellingTagProducts == count($request->selling_tags)) {
                        break;
                    }
                }
            }
        }

        // Update Product Ingredient
        foreach ($request->ingredients as $requestIngredient) {
            $checkProductIngredient = $product->productIngredients()->where('ingredient_id', $requestIngredient['id'])->first();
            if (is_null($checkProductIngredient)) {
                $product->productIngredients()->create([
                    'product_id' => $product->id,
                    'ingredient_id' => $requestIngredient['id'],
                    'quantity' => $requestIngredient['quantity']
                ]);
            }
        }

        $productIngredients = $product->productIngredients()->get();

        if (count($productIngredients) > count($request->ingredients)) {
            foreach ($productIngredients as $productIngredient) {
                $check = false;
                foreach ($request->ingredients as $requestIngredient) {
                    if ($requestIngredient['id'] == $productIngredient['id']) {
                        $check = true;
                        break;
                    }
                }
                if (!$check) {
                    $productIngredient->delete();
                    $countProductIngredients = $product->productIngredients()->count();
                    if ($countProductIngredients == count($request->ingredients)) {
                        break;
                    }
                }
            }
        }

        return response()->json($product, 200);
    }

    public function destroyProduct(Product $product)
    {
        $product->productIngredients()->delete();
        $product->sellingTagProducts()->delete();
        $product->delete();

        return response()->json($product, 200);
    }

    public function getProductNumber(Request $request)
    {
        $lastProduct = Product::where('company_id', $request->company_id)->orderBy('id', 'DESC')->first();

        if (!is_null($lastProduct)) {
            $number = substr($lastProduct->number, -6);
            $addNumber = (int) $number + 1;
            $productNumber = 'PR' . date('dmY') . sprintf("%06d", $addNumber);
        } else {
            $productNumber = 'PR' . date('dmY') . '000001';
        }

        return $productNumber;
    }

    public function getProductById(Product $product)
    {
        $sellingTags = $product->sellingTagProducts()->with(['sellingTag'])->get();
        $product->sellingTags = $sellingTags;
        $ingredients = $product->productIngredients()->with(['ingredient'])->get();
        $product->ingredients = $ingredients;

        return response()->json($product, 200);
    }

    public function getMostSold(Request $request){
        $year = $request->query('year', Carbon::now()->format('Y'));
        $limit = $request->query('limit', 5);

        return DB::table('transactions')
        ->whereYear('transactions.created_at', $year)
        ->join('transaction_products', 'transactions.id', '=', 'transaction_products.id')
        ->join('products', 'products.id', '=', 'transaction_products.product_id')
        ->select('products.name', DB::raw('SUM(quantity) as total_quantity'))
        ->groupBy('products.name')
        ->orderByDesc('total_quantity')
            ->limit($limit)
        ->get();
    }

    public function totalSold(Request $request){
        $year = $request->query('year', Carbon::now()->format('Y'));

        return DB::table('transactions')
        ->whereYear('transactions.created_at', $year)
        ->join('transaction_products', 'transactions.id', '=', 'transaction_products.id')
        ->select( DB::raw('SUM(transaction_products.quantity) as total_sold'))
        ->get();
    }
}
