<?php

namespace App\Services;

use App\Models\SellingTag;
use Illuminate\Http\Request;
use Validator;

class SellingTagService 
{
    public function getSellingTags(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $sellingTags = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $sellingTags = SellingTag::where('name', 'LIKE', '%' . $search . '%')
                    ->where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $sellingTags = SellingTag::where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $sellingTags = SellingTag::where('company_id', $companyId)->orderBy('name', 'ASC')->get();
        }

        return response()->json($sellingTags, 200);
    }

    public function createSellingTag(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $sellingTag = SellingTag::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json($sellingTag, 200);
    }

    public function updateSellingTag(SellingTag $sellingTag, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'company_id' => 'required|integer',
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $sellingTag->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json($sellingTag, 200);
    }

    public function destroySellingTag(SellingTag $sellingTag)
    {
        $sellingTag->delete();

        return response()->json($sellingTag, 200);
    }
}