<?php

namespace App\Services;

use App\Models\Customer;
use Illuminate\Http\Request;
use Validator;

class CustomerService
{
    public function getCustomers(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $customers = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $customers = Customer::with(['company'])
                    ->where('name', 'LIKE', '%' . $search . '%')
                    ->where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $customers = Customer::with(['company'])
                    ->where('company_id', $companyId)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $customers = Customer::with(['company'])->orderBy('name', 'ASC')->get();
        }

        return response()->json($customers, 200);
    }

    public function createCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'phone_number' => 'required|string',
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $customer = Customer::create([
            'company_id' => $request->company_id,
            'phone_number' => $request->phone_number,
            'name' => $request->name,
        ]);

        return response()->json($customer, 200);
    }

    public function updateCustomer(Customer $customer, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'phone_number' => 'required|string',
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $customer->update([
            'company_id' => $request->company_id,
            'phone_number' => $request->phone_number,
            'name' => $request->name,
        ]);

        return response()->json($customer, 200);
    }

    public function destroyCustomer(Customer $customer)
    {
        $customer->delete();

        return response()->json($customer, 200);
    }
}
