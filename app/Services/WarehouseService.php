<?php

namespace App\Services;

use App\Models\Warehouse;
use Illuminate\Http\Request;
use Validator;

class WarehouseService
{
    public function getWarehouses(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $warehouses = null;

        if ($type == 'paginate') {
            if (isset($search)) {
                $warehouses = Warehouse::with(['company'])
                    ->where('name', 'LIKE', '%' . $search . '%')
                    ->where('company_id', $request->company_id)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $warehouses = Warehouse::with(['company'])
                    ->where('company_id', $request->company_id)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $warehouses = Warehouse::with(['company'])
                ->where('company_id', $request->company_id)
                ->orderBy('name', 'ASC')
                ->get();
        }

        return response()->json($warehouses, 200);
    }

    public function createWarehouse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'name' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $warehouse = Warehouse::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'address' => $request->address
        ]);

        return response()->json($warehouse, 200);
    }

    public function updateWarehouse(Warehouse $warehouse, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'company_id' => 'required|integer',
            'name' => 'required|string',
            'phone_number' => 'required|string',
            'address' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $warehouse->update([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'address' => $request->address
        ]);

        return response()->json($warehouse, 200);
    }

    public function destroyWarehouse(Warehouse $warehouse)
    {
        $warehouse->delete();
        return response()->json($warehouse, 200);
    }
}
