<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\ProductIngredient;
use App\Models\StockLog;
use App\Models\Ingredient;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;


class TransactionService
{
    public function getTransactions(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $status = $request->status;
        $transactions = null;

        if ($type == 'paginate') {
            if (isset($search)) {
                $transactions = Transaction::with(['customer'])
                    ->where('number', 'LIKE', '%'.$search.'%')
                    ->where('company_id', $request->company_id)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $transactions = Transaction::with(['customer'])
                    ->where('company_id', $request->company_id)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate($request->perPage);
            }
        } else if ($type == 'all') {
            $transactions = Transaction::with(['transactionProducts.product'])
                ->where('company_id', $request->company_id)
                ->where(function ($x) use ($status) {
                    if (isset($status)) {
                        $x->where('status', $status);
                    }
                })
                ->orderBy('id', 'ASC')
                ->get();
        }

        return response()->json($transactions, 200);
    }

    public function createTransaction(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'customer_id' => 'sometimes|integer|nullable',
            'number' => 'required|string',
            'payment_method' => 'sometimes|string|nullable',
            'total' => 'sometimes|integer',
            'status' => 'required|string',
            'products.*.id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $transaction = Transaction::create([
            'company_id' => $request->company_id,
            'customer_id' => $request->customer_id,
            'number' => $request->number,
            'total' => $request->total,
            'status' => $request->status
        ]);

        // Add Transaction Product
        foreach ($request->products as $requestProduct) {
            $transaction->transactionProducts()->create([
                'product_id' => $requestProduct['id'],
                'transaction_id' => $transaction->id,
                'quantity' => $requestProduct['quantity']
            ]);
        }

        return response()->json($transaction, 200);
    }

    public function updateTransaction(Transaction $transaction, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'company_id' => 'required|integer',
            'customer_id' => 'sometimes|integer|nullable',
            'number' => 'required|string',
            'payment_method' => 'sometimes|string|nullable',
            'total' => 'sometimes|integer',
            'status' => 'required|string',
            'products.*.id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $transaction->update([
            'customer_id' => $request->customer_id,
            'number' => $request->number,
            'payment_method' => $request->payment_method,
            'total' => $request->total,
            'status' => $request->status
        ]);

        // Update Transaction Product
        foreach ($request->products as $requestProduct) {
            $checkTransactionProduct = $transaction->transactionProducts()->where('product_id', $requestProduct['id'])->first();
            if (is_null($checkTransactionProduct)) {
                $transaction->transactionProducts()->create([
                    'product_id' => $requestProduct['id'],
                    'transaction_id' => $transaction->id,
                    'quantity' => $requestProduct['quantity']
                ]);
            } else {
                $checkTransactionProduct->update([
                    'quantity' => $requestProduct['quantity']
                ]);
            }
        }

        $transactionProducts = $transaction->transactionProducts()->get();

        if (count($transactionProducts) > count($request->products)) {
            foreach ($transactionProducts as $transactionProduct) {
                $check = false;
                foreach ($request->products as $requestProduct) {
                    if ($requestProduct['id'] == $transactionProduct->product_id) {
                        $check = true;
                        break;
                    }
                }
                if (!$check) {
                    $transactionProduct->delete();
                    $countTransactionProducts = $transaction->transactionProducts()->count();
                    if ($countTransactionProducts == count($request->products)) {
                        break;
                    }
                }
            }
        }

        return response()->json($transaction, 200);
    }

    public function destroyTransaction(Transaction $transaction)
    {
        $transaction->update([
            'status' => 'canceled'
        ]);

        return response()->json($transaction, 200);
    }

    public function getTransactionNumber(Request $request)
    {
        $lastTransaction = Transaction::where('company_id', $request->company_id)
            ->orderBy('id', 'DESC')
            ->first();

        if (!is_null($lastTransaction)) {
            $number = substr($lastTransaction->number, -6);
            $addNumber = (int) $number + 1;
            $transactionNumber = 'INV' . date('dmY') . sprintf("%06d", $addNumber);
        } else {
            $transactionNumber = 'INV' . date('dmY') . '000001';
        }

        return $transactionNumber;
    }

    public function checkStockProduct(Request $request)
    {
        $dataMinusStockProduct = [];
        $requestProducts = $request->products;

        foreach ($requestProducts as $requestProduct) {
            $productIngredients = ProductIngredient::with(['ingredient'])->where('product_id', $requestProduct['id'])->get();
            $check = true;
            $dataMinusStockIngredient = [];
            foreach ($productIngredients as $productIngredient) {
                if ($productIngredient->quantity * $requestProduct['quantity'] > $productIngredient->ingredient['stock']) {
                    $check = false;
                    array_push($dataMinusStockIngredient, $productIngredient);
                }
            }

            $requestProduct['dataMinusStockIngredient'] = $dataMinusStockIngredient;

            if (!$check) {
                array_push($dataMinusStockProduct, $requestProduct);
            }
        }

        return response()->json($dataMinusStockProduct, 200);
    }

    public function approveMinusStock(Request $request)
    {
        $user = $request->user();

        if (Hash::check($request->password, $user->password)) {
            return response()->json(true, 200);
        } else {
            return response()->json(false, 200);
        }
    }

    public function getTransactionByNumber(Request $request)
    {
        $transaction = Transaction::with(['customer', 'transactionProducts.product'])
            ->where('number', $request->transactionNumber)
            ->where('company_id', $request->company_id)
            ->first();

        return response()->json($transaction, 200);
    }

    public function confirmPayment(Transaction $transaction, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'customer_id' => 'sometimes|integer|nullable',
            'payment_method' => 'required|string',
            'total' => 'required|integer',
            'status' => 'required|string',
            'point' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $customer = Customer::find($request->customer_id);
        if ($transaction->total != $request->total) {
            $customer->update([
                'point' => 0
            ]);
        }

        if (!is_null($customer)) {
            $customer->update([
                'point' => intval($request->total / 10)
            ]);
        }

        $transaction->update([
            'customer_id' => $request->customer_id,
            'payment_method' => $request->payment_method,
            'total' => $request->total,
            'status' => $request->status,
            'point' => $request->point
        ]);

        // Add to Stock Log
        $transactionProducts = $transaction->transactionProducts()->with(['product.productIngredients.ingredient'])->get();
        foreach ($transactionProducts as $transactionProduct) {
            foreach ($transactionProduct->product['productIngredients'] as $productIngredient) {
                $checkIngredient = Ingredient::find($productIngredient['ingredient_id']);
                StockLog::create([
                    'ingredient_id' => $productIngredient['ingredient_id'],
                    'type' => "Out",
                    'quantity' => $transactionProduct->quantity * $productIngredient['quantity'],
                    'remaining_stock' => $checkIngredient->stock - ($transactionProduct->quantity * $productIngredient['quantity'])
                ]);

                $checkIngredient->update([
                    'stock' => $checkIngredient->stock - ($transactionProduct->quantity * $productIngredient['quantity'])
                ]);
            }
        }

        return response()->json($transaction, 200);
    }

    public function selectOrder(Transaction $transaction, Request $request)
    {
        $selectedTrasanction = Transaction::where('status', 'selected')->first();

        if (!is_null($selectedTrasanction)) {
            $selectedTrasanction->update([
                'status' => 'on-going'
            ]);
        }

        $transaction->update([
            'status' => $request->status
        ]);

        return response()->json($transaction, 200);
    }

    public function getSummary (Request $request){
        $currentYear = Carbon::now()->format('Y');
        $transactions=  DB::table('transactions')
        ->where('transactions.company_id','=',$request->user()->company_id)
        ->whereYear('transactions.created_at', $currentYear)
        ->select(  DB::raw('SUM(transactions.total) as total_sales'), DB::raw("DATE_FORMAT(transactions.created_at, '%m') as month" ))
        ->groupby('month')
        ->get();
        return$transactions;
    }
}
