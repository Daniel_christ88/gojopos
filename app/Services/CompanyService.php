<?php

namespace App\Services;

use App\Models\Company;
use Illuminate\Http\Request;
use Validator;

class CompanyService
{
    public function getCompanys(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $companys = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $companys = Company::where('name', 'LIKE', '%' . $search . '%')
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $companys = Company::orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $companys = Company::orderBy('name', 'ASC')->get();
        }

        return response()->json($companys, 200);
    }

    public function createCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'city' => 'required|string',
            'email' => 'required|email',
            'address' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $company = Company::create([
            'name' => $request->name,
            'city' => $request->city,
            'email' => $request->email,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ]);

        return response()->json($company, 200);
    }

    public function updateCompany(Company $company, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'city' => 'required|string',
            'email' => 'required|email',
            'address' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $company->update([
            'name' => $request->name,
            'city' => $request->city,
            'email' => $request->email,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ]);

        return response()->json($company, 200);
    }

    public function destroyCompany(Company $company)
    {
        $company->delete();

        return response()->json($company, 200);
    }
}
