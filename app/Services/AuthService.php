<?php

namespace App\Services;

use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class AuthService 
{
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $accessToken = Auth::user()->createToken('authToken')->accessToken;
            return response(['user' => Auth::user(), 'access_token' => $accessToken], 200);
        } else {
            return response(['message' => 'Invalid Credentials'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone_number' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string',
            'company_name' => 'required|string',
            'company_address' => 'required|string',
            'company_phone_number' => 'required|string',
            'company_city' => 'required|string',
            'company_email' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $company = Company::create([
            'name' => $request->company_name,
            'address' => $request->company_address,
            'phone_number' => $request->company_phone_number,
            'email' => $request->company_email,
            'city' => $request->company_city
        ]);

        $user = User::create([
            'company_id' => $company->id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone_number' => $request->phone_number
        ]);

        return response()->json($user, 200);
    }

    public function getCurrentUser()
    {
        $user = Auth::user()->load('company');

        return response()->json($user, 200);
    }
}