<?php

namespace App\Services;

use App\Models\Brand;
use Illuminate\Http\Request;
use Validator;

class BrandService 
{
    public function getBrands(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $brands = null;

        if ($type == 'paginate') {
            if (isset($search)) {
                $brands = Brand::where('name', 'LIKE', '%'.$search.'%')
                    ->where('company_id', $request->company_id)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $brands = Brand::where('company_id', $request->company_id)->orderBy('id', 'DESC')->paginate($request->perPage);
            }
        } else if ($type == 'all') {
            $brands = Brand::where('company_id', $request->company_id)->orderBy('id', 'ASC')->get();
        }

        return response()->json($brands, 200);
    }

    public function createBrand(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'name' => 'required|string',
            'description' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $brand = Brand::create([
            "company_id" => $request->company_id,
            "name" => $request->name,
            "description" => $request->description
        ]);

        return response()->json($brand, 200);
    }

    public function updateBrand(Brand $brand, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'name' => 'required|string',
            'description' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $brand->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json($brand, 200);
    }

    public function destroyBrand(Brand $brand)
    {
        $brand->delete();

        return response()->json($brand, 200);
    }
}