<?php

namespace App\Services;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Validator;

class SupplierService
{
    public function getSuppliers(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $suppliers = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $suppliers = Supplier::where('name', 'LIKE', '%' . $search . '%')
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $suppliers = Supplier::
                    orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $suppliers = Supplier::orderBy('name', 'ASC')->get();
        }

        return response()->json($suppliers, 200);
    }

    public function createSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'name' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $supplier = Supplier::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'city' => $request->city,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ]);

        return response()->json($supplier, 200);
    }

    public function updateSupplier(Supplier $supplier, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'company_id' => 'required|integer',
            'name' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $supplier->update([
            'name' => $request->name,
            'city' => $request->city,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ]);

        return response()->json($supplier, 200);
    }

    public function destroySupplier(Supplier $supplier)
    {
        $supplier->delete();

        return response()->json($supplier, 200);
    }
}
