<?php

namespace App\Services;

use App\Models\Promotion;
use Illuminate\Http\Request;
use Validator;

class PromotionService 
{
    public function getPromotions(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $promotions = null;

        if ($type == 'paginate') {
            if (isset($search)) {
                $promotions = Promotion::where('title', 'LIKE', '%'.$search.'%')
                    ->where('company_id', $request->company_id)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $promotions = Promotion::where('company_id', $request->company_id)->orderBy('id', 'DESC')->paginate($request->perPage);
            }
        } else if ($type == 'all') {
            $promotions = Promotion::where('company_id', $request->company_id)->orderBy('id', 'ASC')->get();
        }

        return response()->json($promotions, 200);
    }

    public function createPromotion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'title' => 'required|string',
            'total' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $promotion = Promotion::create([
            'company_id' => $request->company_id,
            'title' => $request->title,
            'description' => $request->description,
            'total' => $request->total,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date
        ]);

        return response()->json($promotion, 200);
    }

    public function updatePromotion(Promotion $promotion, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'company_id' => 'required|integer',
            'title' => 'required|string',
            'total' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $promotion->update([
            'title' => $request->title,
            'description' => $request->description,
            'total' => $request->total,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date
        ]);

        return response()->json($promotion, 200);
    }

    public function destroyPromotion(Promotion $promotion)
    {
        $promotion->delete();

        return response()->json($promotion, 200);
    }
}