<?php

namespace App\Services;

use App\Models\StockLog;
use Illuminate\Http\Request;
use Validator;

class StockLogService
{
    public function getStockLogs(Request $request)
    {
        $type = $request->type;
        $stockLogs = null;

        if ($type == 'paginate') {
            $stockLogs = StockLog::with(['ingredient'])
                ->where('ingredient_id', $request->ingredient_id)
                ->orderBy('id', 'DESC')
                ->paginate($request->perPage);
        }

        return response()->json($stockLogs, 200);
    }

    public function createSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'name' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'phone_number' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $supplier = Supplier::create([
            'company_id' => $request->company_id,
            'name' => $request->name,
            'city' => $request->city,
            'address' => $request->address,
            'phone_number' => $request->phone_number
        ]);

        return response()->json($supplier, 200);
    }
}
