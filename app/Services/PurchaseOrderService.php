<?php

namespace App\Services;

use App\Http\Resources\IngredientPurchaseOrderResource;
use App\Models\Ingredient;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderMovement;
use App\Models\StockLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PurchaseOrderService
{
    public function getPurchaseOrders(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $purchaseOrders = null;

        if ($type == 'paginate') {
            if (isset($search)) {
                $purchaseOrders = PurchaseOrder::with(['company', 'supplier'])
                    ->where('number', 'LIKE', '%' . $search . '%')
                    ->where('company_id', $request->company_id)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $purchaseOrders = PurchaseOrder::with(['company', 'supplier'])
                    ->where('company_id', $request->company_id)
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        }

        return response()->json($purchaseOrders, 200);
    }

    public function createPurchaseOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|integer',
            'warehouse_id' => 'required|integer',
            'supplier_id' => 'required|integer',
            'number' => 'required|string',
            'ingredients.*.id' => 'required|integer',
            'ingredients.*.quantity' => 'required|integer',
            'ingredients.*.price' => 'required|integer',
            'date' => 'required|date_format:Y-m-d',
            'shipping_date' => 'sometimes|date_format:Y-m-d',
            'received_date' => 'sometimes|date_format:Y-m-d',
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $purchaseOrder = DB::transaction(function () use ($request) {
            $purchaseOrder = PurchaseOrder::create([
                'company_id' => $request->company_id,
                'warehouse_id' => $request->input('warehouse_id'),
                'supplier_id' => $request->input("supplier_id"),
                'number' => $request->input("number"),
                'date' => $request->input("date"),
                'shipping_date' => $request->input("shipping_date"),
                'received_date' => $request->input("received_date"),
                'status' => $request->input('status'),
                'total' => $request->total
            ]);

            foreach ($request->input('ingredients') as $ingredientReq) {
                $ingredient = Ingredient::find($ingredientReq['id']);
                if ($ingredient) {
                    $purchaseOrder->ingredients()->attach(
                        $ingredient->id,
                        [
                            'quantity' => $ingredientReq['quantity'],
                            'price' => $ingredientReq['price'],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]
                    );
                }
            }

            return $purchaseOrder;
        });

        return response()->json($purchaseOrder, 200);
    }

    public function updatePurchaseOrder(PurchaseOrder $purchaseOrder, Request $request)
    {
        if (!$purchaseOrder->exists) {
            return response()->json([
                'message' => 'record not found'
            ], 400);
        }
        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required|integer',
            'warehouse_id' => 'required|integer',
            'number' => 'required|string',
            'ingredients.*.id' => 'required|integer',
            'ingredients.*.quantity' => 'required|integer',
            'ingredients.*.price' => 'required|integer',
            'date' => 'required|date_format:Y-m-d',
            'shipping_date' => 'sometimes|date_format:Y-m-d',
            'received_date' => 'sometimes|date_format:Y-m-d',
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        try {
            $result = DB::transaction(function () use ($request, $purchaseOrder) {
                $purchaseOrder->update([
                    'user_id' => $request->user()->id,
                    'warehouse_id' => $request->input('warehouse_id'),
                    'supplier_id' => $request->input("supplier_id"),
                    'number' => $request->input("number"),
                    'date' => $request->input("date"),
                    'shipping_date' => $request->input("shipping_date"),
                    'received_date' => $request->input("received_date"),
                    'payment_date' => $request->input('payment_date'),
                    'status' => $request->input('status')
                ]);

                // delete old data
                $purchaseOrder->purchaseOrderMovements()->delete();
                foreach ($request->input('ingredients') as $ingredientReq) {
                    $ingredient = Ingredient::find($ingredientReq['id']);
                    if ($ingredient) {
                        PurchaseOrderMovement::updateOrCreate(
                            [
                                'purchase_order_id' => $purchaseOrder->id,
                                'ingredient_id' => $ingredient->id
                            ],
                            [

                                'quantity' => $ingredientReq['quantity'],
                                'price' => $ingredientReq['price']
                            ]
                        );
                    }
                }


                return $purchaseOrder;
            });
            $purchaseOrder = $result
                ->with('purchaseOrderMovements')
                ->first();
            return $purchaseOrder;
        } catch (\Throwable $e) {
            return response()->json([
                'message' => $e
            ], 500);
        }
    }

    public function changeStatus(PurchaseOrder $purchaseOrder, Request $request){
        $validator = Validator::make($request->all(), [
            'status' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if ($request->status == 'on-delivery') {
            $purchaseOrder->update([
                'status' => $request->input('status'),
                'payment_date' => Carbon::now()
            ]);
        } else if ($request->status == 'complete') {
            $purchaseOrder->update([
                'status' => $request->input('status'),
                'received_date' => Carbon::now()
            ]);

            // Add to Stock Log
            $purchaseOrderMovements = $purchaseOrder->purchaseOrderMovements()->with(['ingredient'])->get();
            foreach ($purchaseOrderMovements as $purchaseOrderMovement) {
                StockLog::create([
                    'ingredient_id' => $purchaseOrderMovement->ingredient_id,
                    'type' => 'In',
                    'quantity' => $purchaseOrderMovement->quantity,
                    'remaining_stock' => $purchaseOrderMovement->ingredient['stock'] + $purchaseOrderMovement->quantity
                ]);
                $ingredient = Ingredient::find($purchaseOrderMovement->ingredient_id);
                $ingredient->update([
                    'stock' => $ingredient->stock + $purchaseOrderMovement->quantity
                ]);
            }
        } else {
            $purchaseOrder->update([
                'status' => $request->input('status')
            ]);
        }

        return $purchaseOrder;
    }

    public function getPurchaseOrderNumber(Request $request)
    {
        $lastPurchaseOrder = PurchaseOrder::where('company_id', $request->company_id)
            ->orderBy('id', 'DESC')
            ->first();

        if (!is_null($lastPurchaseOrder)) {
            $number = substr($lastPurchaseOrder->number, -6);
            $addNumber = (int) $number + 1;
            $purchaseOrderNumber = 'PO' . date('dmY') . sprintf("%06d", $addNumber);
        } else {
            $purchaseOrderNumber = 'PO' . date('dmY') . '000001';
        }

        return $purchaseOrderNumber;
    }

    public function getPurchaseOrderById($id)
    {
        return PurchaseOrder::with('supplier')->with('warehouse')->find($id);
    }

    public function getPurchaseOrderIngredients(PurchaseOrder  $purchaseOrder): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return IngredientPurchaseOrderResource::collection($purchaseOrder->purchaseOrderMovements()->with('ingredient')->get());
    }

    public function getSummary (Request $request){
        $currentYear = Carbon::now()->format('Y');
        $purchaseOrders =  DB::table('purchase_orders')
        ->where('purchase_orders.company_id','=',$request->user()->company_id)
        ->whereYear('purchase_orders.date', $currentYear)
        ->select(  DB::raw('SUM(purchase_orders.total) as total_purchase'), DB::raw("DATE_FORMAT(purchase_orders.date, '%m') as month" ))
        ->groupby('month')
        ->get();
        return $purchaseOrders;
    }
}
