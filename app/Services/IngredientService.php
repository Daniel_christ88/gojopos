<?php

namespace App\Services;

use App\Models\Ingredient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\DB;

class IngredientService
{
    public function getIngredients(Request $request)
    {
        $search = $request->search;
        $type = $request->type;
        $ingredients = null;
        $companyId = $request->company_id;

        if ($type == 'paginate') {
            if (isset($search)) {
                $ingredients = Ingredient::with(['brand', 'warehouse'])
                    ->whereHas('warehouse', function ($x) use ($companyId) {
                        $x->where('company_id', $companyId);
                    })
                    ->where('name', 'LIKE', '%' . $search . '%')
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            } else {
                $ingredients = Ingredient::with(['brand', 'warehouse'])
                    ->whereHas('warehouse', function ($x) use ($companyId) {
                        $x->where('company_id', $companyId);
                    })
                    ->orderBy('id', 'DESC')
                    ->paginate($request->perPage);
            }
        } else {
            $ingredients = Ingredient::with(['warehouse', 'brand'])
                ->whereHas('warehouse', function ($x) use ($companyId) {
                    $x->where('company_id', $companyId);
                })
                ->orderBy('name', 'ASC')->get();
        }

        return response()->json($ingredients, 200);
    }

    public function createIngredient(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'warehouse_id' => 'required|integer',
            'brand_id' => 'required|integer',
            'name' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $ingredient = Ingredient::create([
            'warehouse_id' => $request->warehouse_id,
            'brand_id' => $request->brand_id,
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json($ingredient, 200);
    }

    public function updateIngredient(Ingredient $ingredient, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'warehouse_id' => 'required|integer',
            'brand_id' => 'required|integer',
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $ingredient->update([
            'warehouse_id' => $request->warehouse_id,
            'brand_id' => $request->brand_id,
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json($ingredient, 200);
    }

    public function destroyIngredient(Ingredient $ingredient)
    {
        $ingredient->delete();

        return response()->json($ingredient, 200);
    }

    public function getMostBoughtIngredient(Request $request)
    {
        $year = $request->query('year', Carbon::now()->format('Y'));
        $limit = $request->query('limit', 5);
        return  DB::table('purchase_orders')
            ->where('purchase_orders.company_id', '=', $request->user()->company_id)
            ->whereYear('purchase_orders.date', $year)
            ->join("purchase_order_movements", "purchase_orders.id", '=', 'purchase_order_movements.purchase_order_id')
            ->join('ingredients', 'ingredients.id', '=', 'purchase_order_movements.ingredient_id')
            ->select(
                'ingredients.name as ingredient_name',
                DB::raw('SUM(purchase_order_movements.quantity) as total_quantity')
            )
            ->groupBy('ingredient_name')
            ->orderByDesc('total_quantity')
            ->limit($limit)
            ->get();
    }

    public function totalBought(Request $request){
        $year = $request->query('year', Carbon::now()->format('Y'));

        return  DB::table('purchase_orders')
        ->where('purchase_orders.company_id', '=', $request->user()->company_id)
        ->whereYear('purchase_orders.date', $year)
        ->join("purchase_order_movements", "purchase_orders.id", '=', 'purchase_order_movements.purchase_order_id')
        ->select( DB::raw('SUM(purchase_order_movements.quantity) as total_quantity'))
        ->get();
    }
}
