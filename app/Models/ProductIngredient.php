<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ProductIngredient
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $product_id
 * @property int $ingredient_id
 * @property-read \App\Models\Ingredient $ingredient
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient newQuery()
 * @method static \Illuminate\Database\Query\Builder|ProductIngredient onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient whereIngredientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductIngredient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ProductIngredient withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ProductIngredient withoutTrashed()
 * @mixin \Eloquent
 */
class ProductIngredient extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'ingredient_id',
        'quantity'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }
}
