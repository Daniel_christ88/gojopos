<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SellingTagProduct
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $product_id
 * @property int $selling_tag_id
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\SellingTag $sellingTag
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct newQuery()
 * @method static \Illuminate\Database\Query\Builder|SellingTagProduct onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct whereSellingTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTagProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SellingTagProduct withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SellingTagProduct withoutTrashed()
 * @mixin \Eloquent
 */
class SellingTagProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'selling_tag_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function sellingTag()
    {
        return $this->belongsTo(SellingTag::class);
    }
}
