<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\StockLog
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $ingredient_id
 * @property string $status
 * @property int $quantity
 * @property int $remaining_stock
 * @property-read \App\Models\WarehouseIngredient $warehouseIngredient
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog newQuery()
 * @method static \Illuminate\Database\Query\Builder|StockLog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereIngredientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereRemainingStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StockLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|StockLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|StockLog withoutTrashed()
 * @mixin \Eloquent
 */
class StockLog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'ingredient_id',
        'type',
        'quantity',
        'remaining_stock'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }
}
