<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'supplier_id',
        'warehouse_id',
        'company_id',
        'date',
        'number',
        'payment_date',
        'shipping_date',
        'received_date',
        'status',
        'total'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date',
        'shipping_date',
        'payment_date',
        'received_date',
    ];

    // Relationship
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'purchase_order_movements','purchase_order_id','ingredient_id')
            ->withPivot(['quantity', 'price']);
    }

    public function purchaseOrderMovements()
    {
        return $this->hasMany(PurchaseOrderMovement::class,'purchase_order_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

}
