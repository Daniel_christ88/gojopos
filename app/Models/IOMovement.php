<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IOMovement extends Model
{
    use HasFactory;

    protected $fillable = [
        'ingredient_id',
        'in_out_stock_id',
        'quantity',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
