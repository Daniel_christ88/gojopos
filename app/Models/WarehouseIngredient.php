<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\WarehouseIngredient
 *
 * @property-read \App\Models\Ingredient $ingredient
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrderMovement[] $purchaseOrderMovements
 * @property-read int|null $purchase_order_movements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RequestOrderMovement[] $requestOrderMovements
 * @property-read int|null $request_order_movements_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StockLog[] $stockLogs
 * @property-read int|null $stock_logs_count
 * @property-read \App\Models\Warehouse $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseIngredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseIngredient newQuery()
 * @method static \Illuminate\Database\Query\Builder|WarehouseIngredient onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|WarehouseIngredient query()
 * @method static \Illuminate\Database\Query\Builder|WarehouseIngredient withTrashed()
 * @method static \Illuminate\Database\Query\Builder|WarehouseIngredient withoutTrashed()
 * @mixin \Eloquent
 */
class WarehouseIngredient extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'warehouse_id',
        'ingredient_id',
        'stock'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }

    public function stockLogs()
    {
        return $this->hasMany(StockLog::class);
    }

    public function purchaseOrderMovements()
    {
        return $this->hasMany(PurchaseOrderMovement::class);
    }

    public function requestOrderMovements()
    {
        return $this->hasMany(RequestOrderMovement::class);
    }
}
