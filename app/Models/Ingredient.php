<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Ingredient
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $brand_id
 * @property int $warehouse_id
 * @property string $name
 * @property string|null $description
 * @property int $stock
 * @property-read \App\Models\Brand $brand
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductIngredient[] $productIngredients
 * @property-read int|null $product_ingredients_count
 * @property-read \App\Models\Warehouse $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient newQuery()
 * @method static \Illuminate\Database\Query\Builder|Ingredient onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ingredient whereWarehouseId($value)
 * @method static \Illuminate\Database\Query\Builder|Ingredient withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Ingredient withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrder[] $purchaseOrders
 * @property-read int|null $purchase_orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InOutStock[] $inOutStocks
 * @property-read int|null $in_out_stocks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PurchaseOrderMovement[] $purchaseOrderMovements
 * @property-read int|null $purchase_order_movements_count
 */
class Ingredient extends Model
{
    use SoftDeletes;

    protected $fillable = [

        'warehouse_id',
        'brand_id',
        'name',
        'description',
        'price',
        'stock'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function productIngredients()
    {
        return $this->hasMany(ProductIngredient::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function purchaseOrderMovements(){
        return $this->hasMany(PurchaseOrderMovement::class);
    }

    public function inOutStocks(){
        return $this->belongsToMany(InOutStock::class)->using(IOMovement::class);
    }
}
