<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\RolePermission
 *
 * @method static \Illuminate\Database\Eloquent\Builder|RolePermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RolePermission newQuery()
 * @method static \Illuminate\Database\Query\Builder|RolePermission onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|RolePermission query()
 * @method static \Illuminate\Database\Query\Builder|RolePermission withTrashed()
 * @method static \Illuminate\Database\Query\Builder|RolePermission withoutTrashed()
 * @mixin \Eloquent
 */
class RolePermission extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'role_id',
        'permission_id',
        'activated'
    ];

    protected $date = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
