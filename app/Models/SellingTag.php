<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SellingTag
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $company_id
 * @property string $name
 * @property string|null $description
 * @property-read \App\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SellingTagProduct[] $sellingTagProducts
 * @property-read int|null $selling_tag_products_count
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag newQuery()
 * @method static \Illuminate\Database\Query\Builder|SellingTag onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SellingTag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|SellingTag withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SellingTag withoutTrashed()
 * @mixin \Eloquent
 */
class SellingTag extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'name',
        'description'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function sellingTagProducts()
    {
        return $this->hasMany(SellingTagProduct::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
