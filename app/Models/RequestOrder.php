<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\RequestOrder
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RequestOrderMovement[] $requestOrderMovements
 * @property-read int|null $request_order_movements_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|RequestOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestOrder newQuery()
 * @method static \Illuminate\Database\Query\Builder|RequestOrder onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestOrder query()
 * @method static \Illuminate\Database\Query\Builder|RequestOrder withTrashed()
 * @method static \Illuminate\Database\Query\Builder|RequestOrder withoutTrashed()
 * @mixin \Eloquent
 */
class RequestOrder extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'branch_id',
        'status',
        'date'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date'
    ];

    // Relationship
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function requestOrderMovements()
    {
        return $this->hasMany(RequestOrderMovement::class);
    }
}
