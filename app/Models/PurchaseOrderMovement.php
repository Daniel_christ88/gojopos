<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PurchaseOrderMovement
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $purchase_order_id
 * @property int $ingredient_id
 * @property int $quantity
 * @property int $price
 * @property-read \App\Models\PurchaseOrder $purchaseOrder
 * @property-read \App\Models\WarehouseIngredient $warehouseIngredient
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement newQuery()
 * @method static \Illuminate\Database\Query\Builder|PurchaseOrderMovement onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement query()
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement whereIngredientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement wherePurchaseOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PurchaseOrderMovement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|PurchaseOrderMovement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|PurchaseOrderMovement withoutTrashed()
 * @mixin \Eloquent
 * @property-read \App\Models\Ingredient $ingredient
 */
class PurchaseOrderMovement extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'purchase_order_id',
        'ingredient_id',
        'quantity',
        'price'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class);
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }
}
