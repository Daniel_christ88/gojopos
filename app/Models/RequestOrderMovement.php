<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\RequestOrderMovement
 *
 * @property-read \App\Models\RequestOrder $requestOrder
 * @property-read \App\Models\WarehouseIngredient $warehouseIngredient
 * @method static \Illuminate\Database\Eloquent\Builder|RequestOrderMovement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestOrderMovement newQuery()
 * @method static \Illuminate\Database\Query\Builder|RequestOrderMovement onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|RequestOrderMovement query()
 * @method static \Illuminate\Database\Query\Builder|RequestOrderMovement withTrashed()
 * @method static \Illuminate\Database\Query\Builder|RequestOrderMovement withoutTrashed()
 * @mixin \Eloquent
 */
class RequestOrderMovement extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'request_order_id',
        'warehouse_ingredient_id',
        'quantity'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function requestOrder()
    {
        return $this->belongsTo(RequestOrder::class);
    }

    public function warehouseIngredient()
    {
        return $this->belongsTo(WarehouseIngredient::class);
    }
}
