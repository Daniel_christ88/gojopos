<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InOutStock extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'company_id',
        'number',
        'type',
        'notes'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function ingredients(){
        return $this->belongsToMany(Ingredient::class,'i_o_movements','in_out_stock_id','ingredient_id')
        ->withPivot(['quantity' ]);
    }
}
