<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TransactionProduct
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $product_id
 * @property int $transaction_id
 * @property int $quantity
 * @property int $sub_total
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\Transaction $transaction
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct newQuery()
 * @method static \Illuminate\Database\Query\Builder|TransactionProduct onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereSubTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|TransactionProduct withTrashed()
 * @method static \Illuminate\Database\Query\Builder|TransactionProduct withoutTrashed()
 * @mixin \Eloquent
 */
class TransactionProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'transaction_id',
        'quantity'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Relationship
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
