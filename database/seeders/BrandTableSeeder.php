<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');
        $companies = Company::get();
        
        foreach ($companies as $company) {
            for ($i = 0; $i < 11; $i++) {
                Brand::create([
                    'company_id' => $company->id,
                    'name' => $faker->company,
                    'description' => $faker->text
                ]);
            }
        }
    }
}
