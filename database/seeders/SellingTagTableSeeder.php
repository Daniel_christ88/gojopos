<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\SellingTag;
use Illuminate\Database\Seeder;

class SellingTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Company::get();

        foreach ($companies as $company) {
            SellingTag::create([
                'company_id' => $company->id,
                'name' => 'Best Seller',
                'description' => 'This item is the best seller'
            ]);

            SellingTag::create([
                'company_id' => $company->id,
                'name' => 'Recommendation',
                'description' => 'This item is recommendation by this shop'
            ]);

            SellingTag::create([
                'company_id' => $company->id,
                'name' => 'New',
                'description' => 'This item is new'
            ]);
        }
    }
}
