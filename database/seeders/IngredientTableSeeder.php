<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use App\Models\Warehouse;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class IngredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');

        $warehouses = Warehouse::get();

        foreach ($warehouses as $warehouse) {
            for ($i=0; $i < 11; $i++) { 
                Ingredient::create([
                    'warehouse_id' => $warehouse->id,
                    'brand_id' => $i+1,
                    'name' => $faker->name,
                    'description' => $faker->text,
                    'stock' => $faker->numberBetween(1000, 10000)
                ]);
            }
        }
    }
}
