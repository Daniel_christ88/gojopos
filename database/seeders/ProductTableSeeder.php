<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Company;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\SellingTag;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = new Request();
        $faker = Faker::create('id-ID');
        $productService = new ProductService();
        $companies = Company::get();

        foreach ($companies as $company) {
            $request->company_id = $company->id;
            $sellingTags = SellingTag::where('company_id', $company->id)->get();
            $ingredients = Ingredient::with(['warehouse'])
                ->whereHas('warehouse', function ($x) use ($company) {
                    $x->where('company_id', $company['id']);
                })
                ->get();

            $product = Product::create([
                'company_id' => $company->id,
                'number' => $productService->getProductNumber($request),
                'name' => 'Ice Coffee',
                'description' => $faker->text,
                'sell_price' => $faker->numberBetween(10000, 30000),
                'image' => '/images/ice-coffee.jpg'
            ]);

            // Add Selling Tag to Product
            foreach ($sellingTags as $sellingTag) {
                $product->sellingTagProducts()->create([
                    'product_id' => $product->id,
                    'selling_tag_id' => $sellingTag->id
                ]);
            }

            // Add Ingredient to Product
            for ($i=0; $i < 3; $i++) { 
                $product->productIngredients()->create([
                    'product_id' => $product->id,
                    'ingredient_id' => $faker->numberBetween($ingredients[0]['id'], $ingredients[(count($ingredients)-1)]['id']),
                    'quantity' => $faker->numberBetween(100, 250)
                ]);
            }

            $product = Product::create([
                'company_id' => $company->id,
                'number' => $productService->getProductNumber($request),
                'name' => 'Ice Red Velvet',
                'description' => $faker->text,
                'sell_price' => $faker->numberBetween(10000, 30000),
                'image' => '/images/ice-red-velvet.jpg'
            ]);

            // Add Selling Tag to Product
            foreach ($sellingTags as $sellingTag) {
                $product->sellingTagProducts()->create([
                    'product_id' => $product->id,
                    'selling_tag_id' => $sellingTag->id
                ]);
            }

            // Add Ingredient to Product
            for ($i=0; $i < 3; $i++) { 
                $product->productIngredients()->create([
                    'product_id' => $product->id,
                    'ingredient_id' => $faker->numberBetween($ingredients[0]['id'], $ingredients[(count($ingredients)-1)]['id']),
                    'quantity' => $faker->numberBetween(100, 250)
                ]);
            }

            $product = Product::create([
                'company_id' => $company->id,
                'number' => $productService->getProductNumber($request),
                'name' => "Ice Green Tea",
                'description' => $faker->text,
                'sell_price' => $faker->numberBetween(10000, 30000),
                'image' => '/images/ice-green-tea.jpg'
            ]);

            // Add Ingredient to Product
            for ($i=0; $i < 3; $i++) { 
                $product->productIngredients()->create([
                    'product_id' => $product->id,
                    'ingredient_id' => $faker->numberBetween($ingredients[0]['id'], $ingredients[(count($ingredients)-1)]['id']),
                    'quantity' => $faker->numberBetween(100, 250)
                ]);
            }
        }
    }
}
