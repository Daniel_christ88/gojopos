<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use App\Models\StockLog;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StockLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredients = Ingredient::get();

        foreach ($ingredients as $ingredient) {
            StockLog::create([
                'ingredient_id' => $ingredient->id,
                'type' => 'in',
                'quantity' => $ingredient->stock,
                'remaining_stock' => $ingredient->stock
            ]);
        }
    }
}
