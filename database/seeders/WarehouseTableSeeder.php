<?php

namespace Database\Seeders;

use App\Models\Warehouse;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class WarehouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');

        for ($i=0; $i < 4; $i++) { 
            Warehouse::create([
                'company_id' => $i < 2 ? 1 : 2,
                'name' => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'address' => $faker->address
            ]);
        }
    }
}
