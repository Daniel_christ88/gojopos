<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompanyTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(BrandTableSeeder::class);
        $this->call(WarehouseTableSeeder::class);
        $this->call(IngredientTableSeeder::class);
        $this->call(SellingTagTableSeeder::class);
        $this->call(SupplierTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(PromotionTableSeeder::class);
        $this->call(TransactionTableSeeder::class);
        $this->call(InOutStockTableSeeder::class);
        $this->call(PurchaseOrderTableSeeder::class);
        // $this->call(StockLogTableSeeder::class);
    }
}
