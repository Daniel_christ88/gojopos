<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Ingredient;
use App\Models\InOutStock;
use App\Models\IOMovement;
use App\Services\InOutStockService;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use App\Models\StockLog;

class InOutStockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = new Request();
        $inOutStockService = new InOutStockService();
        $faker = Faker::create('id-ID');
        $companies = Company::get();

        foreach ($companies as $company) {
            $request->company_id = $company->id;
            $inOutStock = InOutStock::create([
                'company_id' => $company->id,
                'number' => $inOutStockService->getInOutStockNumber($request),
                'type' => 'In',
                'notes' => $faker->text
            ]);
            
            for ($i=0; $i < 3; $i++) { 
                $ingredient = Ingredient::with(['warehouse'])
                    ->whereHas('warehouse', function ($x) use ($company) {
                        $x->where('company_id', $company->id);
                    })
                    ->inRandomOrder()
                    ->first();
                $quantity = $faker->numberBetween(1000, 10000);
                IOMovement::create([
                    'ingredient_id' => $ingredient->id,
                    'in_out_stock_id' => $inOutStock->id,
                    'quantity' => $quantity,
                ]);

                // Add to Stock Log
                $checkIngredient = Ingredient::find($ingredient->id);
                StockLog::create([
                    'ingredient_id' => $ingredient->id,
                    'type' => "In",
                    'quantity' => $quantity,
                    'remaining_stock' => $checkIngredient->stock + $quantity
                ]);

                $checkIngredient->update([
                    'stock' => $checkIngredient->stock + $quantity
                ]);
            }
        }
    }
}
