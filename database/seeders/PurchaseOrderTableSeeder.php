<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Ingredient;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderMovement;
use App\Models\StockLog;
use App\Models\Supplier;
use App\Models\Warehouse;
use App\Services\PurchaseOrderService;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Faker\Factory as Faker;
use Carbon\Carbon;

class PurchaseOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = new Request();
        $purchaseOrderService = new PurchaseOrderService();
        $faker = Faker::create('id-ID');
        $companies = Company::get();

        foreach ($companies as $company) {
            $request->company_id = $company->id;
            $warehouse = Warehouse::where('company_id', $company->id)->inRandomOrder()->first();
            $supplier = Supplier::where('company_id', $company->id)->inRandomOrder()->first();
            $purchaseOrder = PurchaseOrder::create([
                'supplier_id' => $supplier->id,
                'warehouse_id' => $warehouse->id,
                'company_id' => $company->id,
                'date' => Carbon::now(),
                'number' => $purchaseOrderService->getPurchaseOrderNumber($request),
                'payment_date' => Carbon::now()->addDays(7),
                'shipping_date' => Carbon::now()->addDays(7),
                'received_date' => Carbon::now()->addDays(14),
                'status' => 'complete',
                'total' => 0
            ]);

            $total = 0;
            for ($i=0; $i < 3; $i++) { 
                // Add Purchase Order Movement
                $ingredient = Ingredient::with(['warehouse'])
                    ->whereHas('warehouse', function ($x) use ($company) {
                        $x->where('company_id', $company->id);
                    })
                    ->inRandomOrder()
                    ->first();
                $price = $faker->numberBetween(100, 500);
                $quantity = $faker->numberBetween(1000, 10000);
                $purchaseOrderMovement = PurchaseOrderMovement::create([
                    'purchase_order_id' => $purchaseOrder->id,
                    'ingredient_id' => $ingredient->id,
                    'quantity' => $quantity,
                    'price' => $price
                ]);

                // Add To Stock Log
                $purchaseOrderMovement = $purchaseOrderMovement->with(['ingredient'])->first();
                StockLog::create([
                    'ingredient_id' => $purchaseOrderMovement->ingredient_id,
                    'type' => 'In',
                    'quantity' => $purchaseOrderMovement->quantity,
                    'remaining_stock' => $purchaseOrderMovement->ingredient['stock'] + $purchaseOrderMovement->quantity
                ]);

                $ingredient = Ingredient::find($purchaseOrderMovement->ingredient_id);
                $ingredient->update([
                    'stock' => $ingredient->stock + $purchaseOrderMovement->quantity
                ]);
                $total += $price * $quantity;
            }

            $purchaseOrder->update([
                'total' => $total
            ]);
        }
    }
}
