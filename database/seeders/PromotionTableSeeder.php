<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use App\Models\Promotion;
use Faker\Factory as Faker;
use Carbon\Carbon;

class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');
        $companies = Company::get();

        foreach ($companies as $company) {
            for ($i = 0; $i < 11; $i++) {
                Promotion::create([
                    'company_id' => $company->id,
                    'title' => $faker->company,
                    'description' => $faker->text,
                    'total' => $faker->numberBetween(10000, 50000),
                    'start_date' => Carbon::now()->subDays(1),
                    'end_date' => Carbon::now()->subDays(7)
                ]);
            }
        }
    }
}
