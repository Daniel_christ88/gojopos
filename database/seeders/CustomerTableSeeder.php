<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Customer;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');
        $companies = Company::get();

        foreach ($companies as $company) {
            for ($i=0; $i < 5; $i++) { 
                Customer::create([
                    'name' => $faker->name,
                    'phone_number' => $faker->phoneNumber,
                    'point' => $faker->numberBetween(0, 25000),
                    'company_id' => $company->id
                ]);
            }
        }
    }
}
