<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Supplier;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');
        $companies = Company::get();

        foreach ($companies as $company) {
            Supplier::create([
                'company_id' => $company->id,
                'name' => $faker->name,
                'city' => $faker->city,
                'address' => $faker->address,
                'phone_number' => $faker->phoneNumber
            ]);

            Supplier::create([
                'company_id' => $company->id,
                'name' => $faker->name,
                'city' => $faker->city,
                'address' => $faker->address,
                'phone_number' => $faker->phoneNumber
            ]);
        }
    }
}
