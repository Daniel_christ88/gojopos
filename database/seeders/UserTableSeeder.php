<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');

        for ($i=0; $i < 2; $i++) { 
            User::create([
                'company_id' => ($i+1),
                'name'      => $faker->name,
                'email'     => 'admin'.($i+1).'@gmail.com',
                'password'  => Hash::make('Admin123!'),
                'phone_number' => $faker->phoneNumber,
            ]);
        }
    }
}
