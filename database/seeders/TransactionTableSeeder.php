<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\StockLog;
use App\Models\Transaction;
use App\Services\TransactionService;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Http\Request;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = new Request();
        $companies = Company::get();
        $faker = Faker::create('id-ID');
        $transactionService = new TransactionService();

        foreach ($companies as $company) {
            for ($i=0; $i < 10; $i++) { 
                $request->company_id = $company->id;
                $quantity = $faker->numberBetween(1, 10);
                $product = Product::where('company_id', $company->id)->inRandomOrder()->first();

                $transaction = Transaction::create([
                    'customer_id' => null,
                    'company_id' => $company->id,
                    'number' => $transactionService->getTransactionNumber($request),
                    'payment_method' => 'Cash',
                    'total' => $quantity * $product->sell_price,
                    'status' => 'on-going'
                ]);

                $transactionProduct = $transaction->transactionProducts()->create([
                    'product_id' => $product->id,
                    'transaction_id' => $transaction->id,
                    'quantity' => $quantity
                ]);

                $transactionProducts = $transaction->transactionProducts()->with(['product.productIngredients.ingredient'])->get();

                // Add to Stock Log
                foreach ($transactionProducts as $transactionProduct) {
                    foreach ($transactionProduct->product['productIngredients'] as $productIngredient) {
                        $checkIngredient = Ingredient::find($productIngredient['ingredient_id']);
                        StockLog::create([
                            'ingredient_id' => $productIngredient['ingredient_id'],
                            'type' => "Out",
                            'quantity' => $transactionProduct->quantity * $productIngredient['quantity'],
                            'remaining_stock' => $checkIngredient->stock - ($transactionProduct->quantity * $productIngredient['quantity'])
                        ]);

                        $checkIngredient->update([
                            'stock' => $checkIngredient->stock - ($transactionProduct->quantity * $productIngredient['quantity'])
                        ]);
                    }
                }
            }
        }
    }
}
