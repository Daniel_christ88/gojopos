<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');

        for ($i=0; $i < 2; $i++) { 
            Company::create([
                'name' => $faker->company,
                'city' => $faker->city,
                'email' => $faker->companyEmail,
                'address' => $faker->address,
                'phone_number' => $faker->phoneNumber
            ]);
        }
    }
}
