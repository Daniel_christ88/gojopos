<?php

namespace Database\Seeders;

use App\Models\PurchaseOrder;
use App\Services\PurchaseOrderService;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;

class PurchaseOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $request = new Request();
        $purchaseOrderService = new PurchaseOrderService();

        $today = Carbon::now();
        for ($i = 0; $i < 9; $i++) {

            if ($i % 2 == 0) {
                PurchaseOrder::create([
                    'supplier_id' => 1,
                    'user_id' => 1,
                    'number' => $purchaseOrderService->getPurchaseOrderNumber($request),
                    'shipping_date' => $today->format('Y-m-d'),
                    'status' => 'on_delivery',
                    'payment_date' => $today->add('days',7)->format('Y-m-d')

                ]);
            } else {
                PurchaseOrder::create([
                    'supplier_id' => 2,
                    'user_id' => 2,
                    'number' => $purchaseOrderService->getPurchaseOrderNumber($request),
                    'shipping_date' => $today->add('days', 7)->format('Y-m-d'),
                    'status' => 'new_order',
                    'payment_date' => $today->add('days',7)->format('Y-m-d')
                ]);
            }



        }
    }
}
