<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id-ID');
        for ($i = 0; $i < 11; $i++) {
            Ingredient::create([
                'brand_id' => $i, // same as brand : 11
                'name' => $faker->company,
                'description' => $faker->text,
                'price' => 1000*$i
            ]);
        }
    }
}
