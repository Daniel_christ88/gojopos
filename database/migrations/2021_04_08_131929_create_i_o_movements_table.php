<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIOMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_o_movements', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('ingredient_id')->references('id')->on('ingredients');
            $table->foreignId('in_out_stock_id')->references('id')->on('in_out_stocks');
            $table->unsignedBigInteger('quantity');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_o_movements');
    }
}
