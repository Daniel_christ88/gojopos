import router from './router'
import Vue from 'vue'
import Buefy from 'buefy'
import './components/validators'
import store from './store'
import { ValidationProvider, ValidationObserver } from 'vee-validate'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas, faLabe } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
library.add(fas);

Vue.component('FontAwesomeIcon', FontAwesomeIcon)
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
Vue.config.productionTip = false
Vue.use(Buefy, {
  defaultIconComponent: FontAwesomeIcon,
  defaultIconPack: 'fas'
})

// Import Component
import App from '../views/vue/App';

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

