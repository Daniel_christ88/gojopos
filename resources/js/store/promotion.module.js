import ApiService from '../common/api.service'

const promotion = {
  actions: {
    fetchIndexPromotion(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/promotion?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    savePromotion(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/promotion`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditPromotion(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/promotion/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    deletePromotion(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.delete(`/promotion/${credentials.id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getPromotionById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/promotion/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
  }
}

export default promotion