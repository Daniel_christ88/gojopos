import ApiService from '../common/api.service'

const brand = {
  actions: {
    fetchIndexBrand(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/brand?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveBrand(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/brand`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditBrand(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/brand/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    deleteBrand(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.delete(`/brand/${credentials.id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getBrandById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/brand/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
  }
}

export default brand