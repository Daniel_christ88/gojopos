
import ApiService from '../common/api.service'

const ingredient = {
  actions: {
    fetchIndexIngredient(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/ingredient?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    fetchMostBoughtIngredient(context,credentials){
        return new Promise((resolve, reject) => {
            ApiService.init()
            ApiService.get(`/ingredient/most-bought?year=${credentials.year}&limit=${credentials.limit}`).then(
              response => {
                if (response.status === 200) {
                  resolve(response)
                }
              },
              error => {
                reject(error)
              }
            )
          });
    },
    fetchTotalBoughtIngredient(context,credentials){
        return new Promise((resolve, reject) => {
            ApiService.init()
            ApiService.get(`/ingredient/total-bought?year=${credentials.year}}`).then(
              response => {
                if (response.status === 200) {
                  resolve(response)
                }
              },
              error => {
                reject(error)
              }
            )
          });
    },
    saveIngredient(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/ingredient`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditIngredient(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/ingredient/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    deleteIngredient(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.delete(`/ingredient/${credentials.id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getIngredientById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/ingredient/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
  }
}

export default ingredient
