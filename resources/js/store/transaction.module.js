import ApiService from '../common/api.service'

const transaction = {
  actions: {
    fetchIndexTransaction(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/transaction?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}&status=${credentials.status}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    fetchTransactionSummary(context, credentials) {
        return new Promise((resolve, reject) => {
          ApiService.init()
          ApiService.get(`/transaction/summary?year=${credentials.year}&limit=${credentials.limit}}`).then(
            response => {
              if (response.status === 200) {
                resolve(response)
              }
            },
            error => {
              reject(error)
            }
          )
        });
      },

    saveTransaction(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/transaction`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditTransaction(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/transaction/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    deleteTransaction(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.delete(`/transaction/${credentials.id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getTransactionById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/transaction/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    fetchDataTransactionByNumber(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/transaction/${credentials.number}/show-by-number?company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getTransactionNumber(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/transaction/get-transaction-number`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    checkStockProduct(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/transaction/check-stock-product`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    approveMinusStockProduct(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init();
        ApiService.post('/transaction/approve-minus-stock', credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response);
            }
          },
          error => {
            reject(error);
          }
        );
      });
    },
    confirmTransaction(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init();
        ApiService.put(`/transaction/${credentials.id}/confirm-payment`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response);
            }
          },
          error => {
            reject(error);
          }
        );
      });
    },
    selectOrder(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init();
        ApiService.put(`/transaction/${credentials.id}/select-order`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response);
            }
          },
          error => {
            reject(error);
          }
        );
      });
    }
  }
}

export default transaction
