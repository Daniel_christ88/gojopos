
import ApiService from '../common/api.service'

const customer = {
  actions: {
    fetchIndexCustomer(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/customer?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveCustomer(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/customer`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditCustomer(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/customer/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    deleteCustomer(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.delete(`/customer/${credentials.id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getCustomerById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/customer/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
  }
}

export default customer
