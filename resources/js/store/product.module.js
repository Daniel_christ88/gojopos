import ApiService from '../common/api.service'

const product = {
  actions: {
    fetchIndexProduct(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/product?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    fetchProductMostSold(context, credentials) {
        return new Promise((resolve, reject) => {
          ApiService.init()
          ApiService.get(`/product/most-sold?year=${credentials.year}&limit=${credentials.limit}}`).then(
            response => {
              if (response.status === 200) {
                resolve(response)
              }
            },
            error => {
              reject(error)
            }
          )
        });
      },
      fetchProductTotalSold(context, credentials) {
        return new Promise((resolve, reject) => {
          ApiService.init()
          ApiService.get(`/product/total-sold?year=${credentials.year}`).then(
            response => {
              if (response.status === 200) {
                resolve(response)
              }
            },
            error => {
              reject(error)
            }
          )
        });
      },
    saveProduct(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/product`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditProduct(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/product/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    deleteProduct(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.delete(`/product/${credentials.id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getProductById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/product/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    getProductNumber(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/product/get-product-number`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    uploadProductImage(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.postFormData(`/product/upload-image`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    }
  }
}

export default product
