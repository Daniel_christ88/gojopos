import ApiService from '../common/api.service'
import JwtService from '../common/jwt.service'

const state = {
  errors: null,
  message: null,
  user: {},
  loggedIn: !!JwtService.getToken()
}

const getters = {
  currentUser (state) {
    return state.user
  },
  isLoggedIn (state) {
    return state.loggedIn
  }
}

const actions = {
  getCurrentUser (context) {
    return new Promise((resolve, reject) => {
      ApiService.init()
      ApiService.get('user/current').then(
        response => {
          if (response.status == 200) {
            context.commit('setAuth', response.data)
            resolve(response)
          }
        },
        error => {
          context.commit('purgeAuth')
          reject(error)
        }
      )
    })
  },
  login (context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.init()
      ApiService.post('/login', credentials).then(
        response => {
          if (response.status == 200) {
            context.commit('setAccessToken', response.data.accessToken)
            context.commit('setAuth', response.data.user)
          }
          resolve(response)
        },
        error => {
          reject(error)
        }
      )
    })
  },
  register(context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.init()
      ApiService.post('/register', credentials).then(
        response => {
          if (response.status == 200) {
            resolve(response)
          }
        },
        error => {
          reject(error)
        }
      )
    })
  },
  logout (context) {
    context.commit('purgeAuth')
  },
}

const mutations = {
  setError (state, error) {
    state.errors = error
  },
  setAuth (state, user) {
    state.loggedIn = true
    state.user = user
    state.errors = {}
  },
  setisLoggedIn () {
    state.loggedIn = true
  },
  setAccessToken (state, data) {
    JwtService.saveToken(data)
  },
  purgeAuth (state) {
    state.loggedIn = false
    state.errors = {}
    JwtService.destroyToken()
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
