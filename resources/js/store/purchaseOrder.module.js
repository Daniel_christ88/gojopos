import ApiService from '../common/api.service'

const purchaseOrder = {
  actions: {
    fetchIndexPurchaseOrder(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/purchase-order?perPage=${credentials.perPage}&page=${credentials.page}&search=${credentials.search}&type=${credentials.type}&company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    fechPurchaseorderSummary(context, credentials){
        return new Promise((resolve, reject) => {
            ApiService.init()
            ApiService.get(`/purchase-order/summary?year=${credentials.year}`).then(
              response => {
                if (response.status === 200) {
                  resolve(response)
                }
              },
              error => {
                reject(error)
              }
            )
          });
    },
    fetchPurchaseOrderIngredients(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/purchase-order/${credentials.id}/show/ingredients`).then(
          response => {
            resolve(response)
          },
          error => {
            reject(error)
          }
        )
      })
    },
    fetchPurchaseOrderNumber(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/purchase-order/get-purchase-order-number?company_id=${credentials.company_id}`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    savePurchaseOrder(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.post(`/purchase-order`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    fetchPurchaseOrderById(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.get(`/purchase-order/${credentials.id}/show`).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    saveEditPurchaseOrder(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/purchase-order/${credentials.id}`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
    changeStatusPurchaseOrder(context, credentials) {
      return new Promise((resolve, reject) => {
        ApiService.init()
        ApiService.put(`/purchase-order/${credentials.id}/change-status`, credentials).then(
          response => {
            if (response.status === 200) {
              resolve(response)
            }
          },
          error => {
            reject(error)
          }
        )
      });
    },
  }
}

export default purchaseOrder
