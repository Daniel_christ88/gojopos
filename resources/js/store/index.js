import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth.module'
import brand from './brand.module'
import ingredient from './ingredient.module'
import warehouse from './warehouse.module'
import sellingTag from './sellingTag.module'
import supplier from './supplier.module'
import product from './product.module'
import purchaseOrder from "./purchaseOrder.module";
import promotion from './promotion.module'
import transaction from './transaction.module'
import customer from './customer.module'
import company from './company.module'
import inOutStock from './inOutStock.module'
import stockLog from './stockLog.module'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,
        brand,
        ingredient,
        warehouse,
        sellingTag,
        supplier,
        product,
        purchaseOrder,
        promotion,
        transaction,
        customer,
        company,
        inOutStock,
        stockLog,
    }
})
