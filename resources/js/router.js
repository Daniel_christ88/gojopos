import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store/index.js";
// Import Component Layout
import Home from "../views/vue/Home";
import Login from "../views/vue/Login";
import Register from "../views/vue/Register";
import Dashboard from "../views/vue/Dashboard";

// Purchase Order
import PurchaseOrder from "../views/vue/purchaseOrder/PurchaseOrder";

// Master Data
import MasterData from "../views/vue/master-data/MasterData";
  // Ingredient
import IndexIngredient from "../views/vue/master-data/ingredient/IndexIngredient";
  // Product
import IndexProduct from "../views/vue/master-data/product/IndexProduct";
  // Brand
import IndexBrand from "../views/vue/master-data/brand/IndexBrand";
  // Warehouse
import IndexWarehouse from "../views/vue/master-data/warehouse/IndexWarehouse";
  // Selling Tag
import IndexSellingTag from "../views/vue/master-data/selling-tag/IndexSellingTag";
  // Supplier
import IndexSupplier from "../views/vue/master-data/supplier/IndexSupplier";
  // Promotion
import IndexPromotion from "../views/vue/master-data/promotion/IndexPromotion";

// Transaction
import Transaction from "../views/vue/transaction/Transaction";
  // Transaction Order
import IndexTransactionOrder from "../views/vue/transaction/order/IndexOrder";
  // Transaction History
import IndexTransactionHistory from "../views/vue/transaction/history/IndexHistory";
  // Transaction Payment
import IndexTransactionPayment from "../views/vue/transaction/payment/IndexPayment";
  // Transaction Detail Payment
import DetailTransactionPayment from "../views/vue/transaction/payment/DetailPayment";

// User Management
import User from "../views/vue/userManagement/User";
  // Customer
import Customer from "../views/vue/userManagement/Customer";

// Stock Management
import StockManagement from "../views/vue/stockManagement/StockManagement";
  // In Out Stock
import InOutStock from "../views/vue/stockManagement/InOutStock/InOutStock";
  // Stock Log
import IndexStockLog from '../views/vue/stockManagement/stockLog/IndexStockLog';

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    component: Login,
    name: "Login"
  },
  {
    path: "/register",
    component: Register,
    name: "Register"
  },
  {
    path: "/",
    component: Home,
    name: "Home",
    redirect: "/dashboard",
    children: [
      // Dashboard
      {
        path: "/dashboard",
        name: "Dashboard",
        component: Dashboard,
        meta: {
          requiresAuth: true,
          name: "Dashboard"
        }
      },
      {
        path: "/purchase-order",
        name: "PurchaseOrder",
        component: PurchaseOrder,
        meta: {
          requiresAuth: true,
          name: "PurchaseOrder"
        }
      },
      // User Management
      {
        path: "/user",
        component: User,
        meta: { requiresAuth: true },
        children: [
          {
            path: "",
            name: "Customer",
            component: Customer,
            meta: { requiresAuth: true }
          }
        ]
      },
      // Master Data
      {
        path: "/master-data",
        component: MasterData,
        meta: { requiresAuth: true },
        children: [
          {
            path: "",
            name: "MasterData.IndexIngredient",
            component: IndexIngredient,
            meta: { requiresAuth: true }
          },
          {
            path: "/master-data/product",
            name: "MasterData.IndexProduct",
            component: IndexProduct,
            meta: { requiresAuth: true }
          },
          {
            path: "/master-data/brand",
            name: "MasterData.IndexBrand",
            component: IndexBrand,
            meta: { requiresAuth: true }
          },
          {
            path: "/master-data/warehouse",
            name: "MasterData.Warehouse",
            component: IndexWarehouse,
            meta: { requiresAuth: true }
          },
          {
            path: "/master-data/selling-tag",
            name: "MasterData.SellingTag",
            component: IndexSellingTag,
            meta: { requiresAuth: true }
          },
          {
            path: "/master-data/supplier",
            name: "MasterData.Supplier",
            component: IndexSupplier,
            meta: { requiresAuth: true }
          },
          {
            path: "/master-data/promotion",
            name: "MasterData.Promotion",
            component: IndexPromotion,
            meta: { requiresAuth: true }
          }
        ]
      },
      // Transaction
      {
        path: "/transaction",
        component: Transaction,
        meta: { requiresAuth: true },
        children: [
          {
            path: "",
            name: "Transaction.IndexTransactionOrder",
            component: IndexTransactionOrder,
            meta: { requiresAuth: true }
          },
          {
            path: "/transaction/history",
            name: "Transaction.IndexTransactionHistory",
            component: IndexTransactionHistory,
            meta: { requiresAuth: true }
          },
          {
            path: "/transaction/:transactionNumber/payment",
            name: "Transaction.IndexTransactionPayment",
            component: IndexTransactionPayment,
            meta: { requiresAuth: true }
          },
          {
            path: "/transaction/:transactionNumber/detail-payment",
            name: "Transaction.DetailTransactionPayment",
            component: DetailTransactionPayment,
            meta: { requiresAuth: true }
          },
        ]
      },
      // Stock Management
      {
        path: "/stock",
        component: StockManagement,
        meta: { requiresAuth: true },
        children: [
          {
            path: "",
            name: "StockManagement.InOutStock",
            component: InOutStock,
            meta: { requiresAuth: true }
          },
          {
            path: "/stock/log",
            name: "StockManagement.StockLog",
            component: IndexStockLog,
            meta: { requiresAuth: true }
          },
        ]
      }
    ]
  },
];

const router = new VueRouter({
  mode: "history",
  linkActiveClass: "is-active",
  routes
});

router.beforeEach(async (to, from, next) => {
  if (
    !to.matched.some(record => record.meta.requiresAuth) &&
    store.getters.isLoggedIn
  ) {
    await store.dispatch("getCurrentUser");
    next("/");
  } else if (
    to.matched.some(record => record.meta.requiresAuth) &&
    !store.getters.isLoggedIn
  ) {
    alert("You need to login first");
    next("/login");
  } else if (store.getters.isLoggedIn) {
    await store.dispatch("getCurrentUser").catch(() => {
      next("/login");
    });
    next();
  } else {
    next();
  }
});

export default router;
