const ID_TOKEN_KEY = 'id_gojopos'

export const getToken = () => {
  return localStorage.getItem(ID_TOKEN_KEY)
}

export const saveToken = token => {
  localStorage.setItem(ID_TOKEN_KEY, token)
}

export const destroyToken = () => {
  localStorage.clear()
}

export default { getToken, saveToken, destroyToken }
