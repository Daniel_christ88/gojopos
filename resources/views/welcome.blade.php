<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gojopos</title>
        <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
        <script src="{{ mix('js/app.js') }}" type="text/javascript" defer></script>
    </head>

    <!-- Main Contebt -->
    <body style='background-color: white'>
        <div id="app"></div>
    </body>
</html>
