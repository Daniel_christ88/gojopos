export default [
  {
    title: 'Dashboard',
    icon: 'home',
    to: '/dashboard'
  },
  {
    title: 'Purchase Order',
    icon: 'shopping-cart',
    to: '/purchase-order'
  },
  {
    title: 'Stock Management',
    icon: 'cubes',
    children: [
      {
        title: 'In Out Stock',
        icon: 'layer-group',
        to: '/stock',
      },
      {
        title: 'Stock Log',
        icon: 'dolly',
        to: '/stock/log'
      }
    ]
  },
  {
    title: 'User Management',
    icon: 'users',
    children: [
      {
        title: 'Customer',
        icon: 'user-alt',
        to: '/user',
      },
    ]
  },
  {
    title: 'Transaction',
    icon: 'cash-register',
    children: [
      {
        title: 'Transaction Order',
        icon: 'handshake',
        to: '/transaction',
      },
      {
        title: 'Transaction History',
        icon: 'history',
        to: '/transaction/history',
      },
    ]
  },
  {
    title: 'Master Data',
    icon: 'database',
    children: [
      {
        title: 'Brands',
        icon: 'apple-alt',
        to: '/master-data/brand',
      },
      {
        title: 'Ingredients',
        icon: 'carrot',
        to: '/master-data',
      },
      {
        title: 'Products',
        icon: 'leaf',
        to: '/master-data/product',
      },
      {
        title: 'Promotions',
        icon: 'ticket-alt',
        to: '/master-data/promotion',
      },
      {
        title: 'Selling Tags',
        icon: 'tags',
        to: '/master-data/selling-tag',
      },
      {
        title: 'Suppliers',
        icon: 'parachute-box',
        to: '/master-data/supplier',
      },
      {
        title: 'Warehouses',
        icon: 'warehouse',
        to: '/master-data/warehouse',
      },
    ]
  },
]