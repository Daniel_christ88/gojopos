## How To Initialize Project in First Time
composer install
setup database di env
php artisan migrate
php artisan db:seed
php artisan key:generate
php artisan storage:link
php artisan passport:install
npm install

## How To Run
Use 2 terminal :
- php artisan serve
- npm run watch

## If use php artisan migrate:fresh --seed, dont forget to run php artisan passport:install
